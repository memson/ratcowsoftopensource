This project contains the parts of Rat Cow Software's products that are opensourced.

# Recent additions:

Outliner - a simple, pointless outliner app I wrote because I was too lazy to look for one on the web! Yay :-)

mvcframework - a simple MVC style framework for System.Windows.Forms (Winforms) - in constant development. The current head version s very stable.

mvcframework.Mapping - an API to map arbitrary data (models) to controls (view) and track changes through a subscription model. This code is legacy and is in flux, but in its original form has been used in production for 2+ years.

Added some old Delphi and BeOS code to the repo.

Added a simple ComicBook? file (CBZ and CBR) API. Currently allows opening and traversal of the contents and also has a basic implementation of ComicBookInfo? (see http://code.google.com/p/comicbookinfo/ for more info) and a very, very simple demo app that uses the currently implemented API and the mvcframework code. NB. to gain ZIP and RAR access, the following libraries are used (C#3.5 compatible revisions) :- Ionic.Zip and NUrar and for JSON support Newtonsoft.Json. A copy of each assembly required is included in the repo.

All code now licensed under a version of the BSD license.